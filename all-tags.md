---
styles:
    - href: "main.css"
---

{% assign allTags =  site.notices | map: 'tags' | uniq | sort_natural %}

# Yo ! 

Voici les notices rangées par mot-clef avec tous les mots-clefs

{% for tag in allTags %}

    {% comment %}
        Du "capture" au "assign noticeCount", l'objectif est de trouver le nombre de posts avec
        un certain tag
        Je n'ai pas trouvé mieux en liquid que de générer une chaine de caractère dans un "capture"
        avec un for et un if puis de mesurer la taille de la chaine générée
    {% endcomment %}

    {% capture counter -%}
        {%- for notice in site.notices -%}
            {%- if notice.tags contains tag -%}x{%- endif -%}
        {%- endfor -%}
    {%- endcapture %}

    {% assign noticeCount = counter | strip | remove: " " | size %}

    {% assign detailsId = tag | replace: " ", "-" | prepend: "tag-" %}

<details id="{{ detailsId }}">
    <summary><h2>{{ tag }} ({{ noticeCount }})</h2></summary>
    <ul>
        {% for notice in site.notices %}
            {% if notice.tags contains tag %}
        <li><a href="{{ notice.url | relative_url }}">{{ notice.nom }}</a></li>
            {% endif %}
        {% endfor %}
    </ul>
</details>

{% endfor %}

<style>
    details{
        margin-bottom: 1rem;
    }

    summary > * {
        display: inline;
    }
</style>