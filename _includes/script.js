/*
  J'ai un "score" qui vaut 0 au début
  -> let ça déclare la variable (dont on peut changer la valeur au fur et à mesure, sinon il peut y avoir const)


  (J'ai des data-score)
  Quand je clique sur un élément avec un data-score, (ou plutôt, on fait une liste et on réagit quand c'est un des élements de la liste qui est cliqué)
  -> on va rajouter un event-listener, puis une réaction (un bout de code qui se lance)
  -> il faut trouver un moyen de trouver tous les éléments avec un data-score
  -> on crée une variable pour enregistrer la liste de tous ces éléments
  -> on peut pas nommer les variables avec des - genre "data-Score-Elements" donc on écrit en camelcase dataScoreElements
  -> = ça veut dire "reçoit"
  -> document.querySelectorAll(selector) récupère tout les éléments qui correspondent au sélecteur CSS entre parenthèses (= qui est passé en argument)
  -> a[data-score] tout les éléments <a> avec un attribut data-score (c'est du CSS)

  alors tu fais une addition avec "score"

  Affiche "score" à la fin
*/

  const dataScoreElements = document.querySelectorAll("[data-score]")
  console.log(dataScoreElements)

  const output = document.querySelector("output")
  if (localStorage.getItem('score')) {
    output.textContent = localStorage.getItem('score')
  } else {
    output.textContent = 0
  }

  for(const element of dataScoreElements){
    element.addEventListener("click",function(){
      const elementScore = Number(element.getAttribute("data-score"))
      console.log(elementScore)
      localStorage.setItem('score', Number(localStorage.getItem('score')) + elementScore)
      console.log(score)
    })
  }

/*  Pour chacun des éléments de cette liste = for(const element of dataScoreElements)
    tu prends l'élément et tu récupère le data-score dans elementScore (pour le connaitre en javascript)
    On lui dit aussi avec Number que c'est des nombres sinon il fait 40404040 avec l'addition
*/

/* Output c'est pour afficher le score (à la fin) */


const welcomeButton = document.querySelector("#retourDebut")
welcomeButton.addEventListener("click",function(){
  localStorage.setItem('score', 0)
})

/*  welcomeButton est une variable, et elle récupère le premier élément qui a un id retourDebut
    quand on clique sur la fonction welcomeButton, écoute les évènements click et lance la fonction qui dit que le score est égal à 0
*/
