//@ts-check

import { promisify } from 'util';
import { writeFile } from 'fs/promises';
import { createWriteStream } from 'fs';
import { pipeline as pl } from 'stream';

import got from "got";
import pLimit from 'p-limit';

import {makeLogoFilename} from './commons.js'

const pipeline = promisify(pl);

const limit = pLimit(40);

const CATEGORY_LIST = [
    "Métiers", "CMS", "Bureautique", 
    "Cloud/webApps", "Création", "Développement", 
    "Éducation", "Internet", "Jeux", 
    "Multimédia" , "Science", "Sécurité",
    "Système"
]

const FRAMALIBRE_ORIGIN = 'https://framalibre.org';

const EXPORT_DIRECTORY = './drupal-export'
const LOGO_IMAGE_DIRECTORY = 'images/logo'



console.time('tick')

// @ts-ignore
const allCategoriesDrupal = await got(`${FRAMALIBRE_ORIGIN}/category/annuaires/json`).json()

const allCategories = allCategoriesDrupal.nodes.map(cat => cat.node)

console.log('allCategories', allCategories.map(c => c.Nom))

const allSelectedCategories = CATEGORY_LIST.map(cat => allCategories.find(({Nom}) => cat === Nom))

Promise.all( allSelectedCategories.map(({absolute_url, Nom: categoryName}) => {
    const categoryJSONURL = `${absolute_url}/json`

    return got(categoryJSONURL).json()
    .then(xDrup => {
        const categoryNoticeObjs = xDrup.nodes.map(a => a.node)
        
        console.log('Catégorie', categoryName, categoryNoticeObjs.length)
    
        return Promise.all( categoryNoticeObjs.map(categoryNoticeObj => {
            const categoryNoticeURL = `${FRAMALIBRE_ORIGIN}${categoryNoticeObj.json_id}`
    
            return limit( () => got(categoryNoticeURL).json()
                .then(noticeDrup => noticeDrup.nodes[0].node)
                .then(notice => {
                    notice["Catégorie"] = categoryName;

                    const nom = notice['Titre'];

                    const noticeLogoURL = notice['Logo'].src;

                    if(noticeLogoURL){
                        const logoPath = `${LOGO_IMAGE_DIRECTORY}/${makeLogoFilename(noticeLogoURL, nom)}`
                        
                        console.log('img file', logoPath)

                        const writeStream = createWriteStream(logoPath)

                        return pipeline( 
                            got.stream(noticeLogoURL), 
                            writeStream
                        )
                        .then( () => {
                            notice['Logo'].src = logoPath;

                            return notice 
                        })
                    }
                    else{
                        return notice
                    }
                })
                .catch(err => console.log('notice error', categoryNoticeURL, err)) )
        }) )
    })
}) )
.then(allCategoryNotices => {
    allCategoryNotices = allCategoryNotices.flat()

    console.timeEnd('tick')

    console.log('allCategoryNotices', allCategoryNotices.length)

    const str = JSON.stringify(allCategoryNotices, null, 2)

    return writeFile(`${EXPORT_DIRECTORY}/notices-export.json`, str);
})
.catch(err => console.error)

