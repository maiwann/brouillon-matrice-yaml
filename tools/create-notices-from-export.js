//@ts-check

import { readFile, writeFile } from 'fs/promises';

import {softwareNameToFileName} from './commons.js'

/*

à partir du dossier drupal-export, créer toutes les notices au format attendu

*/


function escapeQuotesInYAMLValue(text){
    return text ? 
        text.trim()
        .replace(/\\/g, '\\\\')
        .replace(/"/g, '\\"') 
        : ''
}

function createNewFormatNotice({
    "Titre": nom,
    "Post date": date_creation,
    "Updated date": date_modification,
    "Étiquettes": mots_clefs,
    "Catégorie": catégorie,
    "Logo": logo,
    "Lien officiel": site_web,
    "Disponible sur": plateformes,
    "Langue": langues,
    "Texte descriptif": description,
    "Créateur(s)": createurices,
    "Licence(s)": licences,
    "Page Wikipédia": lien_wikipedia,
    "Page Exodus Privacy": lien_exodus,
    "body_1": body
}) {
    const tags = new Set([catégorie].concat( mots_clefs.split(',').map(s => s.trim()).filter(t => t.length >= 1) ))

    return `---
layout: notice

nom: "${escapeQuotesInYAMLValue(nom)}"
date_creation: "${escapeQuotesInYAMLValue(date_creation)}"
date_modification: "${escapeQuotesInYAMLValue(date_modification)}"
logo: 
    src: "../${escapeQuotesInYAMLValue(logo.src)}"
site_web: "${escapeQuotesInYAMLValue(site_web)}"
plateformes: "${escapeQuotesInYAMLValue(plateformes)}"
langues: "${escapeQuotesInYAMLValue(langues)}"
description_courte: "${escapeQuotesInYAMLValue(body)}"
createurices: "${escapeQuotesInYAMLValue(createurices)}"
licences: "${escapeQuotesInYAMLValue(licences)}"
tags: 
${[...tags].map(t => `    - "${t}"`).join('\n')}
lien_wikipedia: "${escapeQuotesInYAMLValue(lien_wikipedia)}"
lien_exodus: "${escapeQuotesInYAMLValue(lien_exodus)}"
---

${description}
`
}



readFile('./drupal-export/notices-export.json', { encoding: 'utf-8' })
.then(JSON.parse)
.then(exportedData => {
    console.log('exportedData', exportedData.length)

    return Promise.all(exportedData.map(notice => {
        const formattedNotice = createNewFormatNotice(notice);

        console.log('Création notice', softwareNameToFileName(notice.Titre, '.md'))

        return writeFile(`./_notices/${softwareNameToFileName(notice.Titre, '.md')}`, formattedNotice, 'utf-8')
    }))

})


