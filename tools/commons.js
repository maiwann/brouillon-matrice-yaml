
import { basename, extname } from 'path';

export function softwareNameToFileName(softwareName, extension){
    if(!extension)
        throw new Error('Missing extension in softwareNameToFileName')

    return softwareName
      .trim()
      .replace(/\//g, '-') 
      + extension
}

export function makeLogoFilename(drupalExportURL, nom){
    return softwareNameToFileName(nom, extname(basename(drupalExportURL)))
}