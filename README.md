# Matrice Designers Ethiques

## Intention

Tout est expliqué sur le site des [Designers Éthiques](https://attention.designersethiques.org/)

## Guide d'écriture sur Airtable 

- N'utiliser que des guillemets français `« »`
- Dans les réponses multiples, il faut faire très attention à l'identation des réponses (sinon ça affiche une page blanche) : 
   - Un tiret + une espace pour libelle : `- libelle: "Contenu"`
   - Deux espaces pour score : `  score: 10`


## Format des données pour la moulinette Airtable -> Format YAML

à ranger dans le dossier `_questions` avec la convention de nommage suivante :
CodeApiDeLaQuestion.md

Le contenu des fichiers est décrit de la façon suivante :
`Format YAML` : Contenu de la colonne Airtable citée

### Si le type de question est OUI/NON

---

`layout`: question_oui_non <!-- Pour toutes les questions -->

`type`: oui_non <!-- Pour toutes les questions -->

`code_api`: Code API

`libelle`: Proposition Wording Inclusif Yuna<!-- ou Question -->

`description`: Description

`exemples`: Exemples

`score`: Importance

`chemin_suivant`: chemin_suivant

`chemin_non`: chemin_non

---

### Si le type de question est MULTIPLE

---

`layout`: question_multiple <!-- Pour toutes les questions -->

`type`: multiple <!-- Pour toutes les questions -->

`code_api`: Code API

`libelle`: Proposition Wording Inclusif Yuna<!-- ou Question -->

`chemin_suivant`: chemin_suivant

`description`: Description

`exemples`: Exemples

`reponses`: Réponses multiples

---

## Licence 

La matrice est sous licence CC-by
Pour plus d'infos : le site des [Designers Éthiques](https://attention.designersethiques.org/)
