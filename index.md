---
styles:
    - href: "main.css"
---

<section id="welcome">


  <h2>Testez un site ou une application pour savoir si elle respecte votre attention !</h2>
  <span> Designers Ethiques - CCBy - V.0.1</span>

  <hr>

  <a href="https://maiwann.frama.io/brouillon-matrice-yaml/questions/1A.html" class="btn">Démarrer</a>
</section>
