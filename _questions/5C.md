---
layout: question_oui_non
type: oui_non
code_api: "5C"
libelle: "Le service affiche des comparaisons de prix ou de chiffres."
description: "Nous ciblons ici les offres promotionnelles ou soldes ou toutes comparaisons de montants qui a pour objectif de faire croire / comprendre à l'utilisateur qu'il va faire une bonne affaire"
exemples: "Oui.sncf : « 6 places restantes à ce prix »"
score: "50"
chemin_suivant: "5D"
chemin_non: "5D"
---