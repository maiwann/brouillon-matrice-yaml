---
layout: question_oui_non
type: oui_non
code_api: "1A3a"
libelle: "Les fonctionnalités de partage suivent la navigation de la personne utilisatrice."
description: "Les boutons de partage qui suivent l'utilisateur sont peu problématique car ils offrent une possibilité à l'utilisateur sans l'inciter ou le contraindre à partager."
score: "10"
chemin_suivant: "1A3b"
chemin_non: "1A3b"
---