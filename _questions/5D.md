---
layout: question_oui_non
type: oui_non
code_api: "5D"
libelle: "Le service attire l'attention par des images contrastées."
description: "Exemple de Youtube où certaines Youtubeurs contrastent fortement les vignettes : est-ce qu'on sanctionne Youtube pour ça ?"
score: "30"
chemin_suivant: "5E"
chemin_non: "5E"
---