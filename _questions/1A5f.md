---
layout: question_oui_non
type: oui_non
code_api: "1A5f"
libelle: "Le service envoie des notifications qui n'apportent pas de valeur ajoutée."
description: "Les notifications sont considérées comme abusives lorsqu'elles n'ont pas vraiment lieu d'être, car en décalage total avec le service ou signalent un élément évident à l'utilisateur."
exemples: "Par exemple, en envoyant une notification à 20h signalant le journal de 20h. https://www.generation-nt.com/google-chrome-notification-spam-protection-actualite-1976774.html"
score: "80"
chemin_suivant: "1A5f1"
chemin_non: "1A5f1"
---