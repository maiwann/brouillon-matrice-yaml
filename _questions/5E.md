---
layout: question_oui_non
type: oui_non
code_api: "5E"
libelle: "Le service fait ou encourage un usage abusif des majuscules."
exemples: "YouTube"
score: "20"
chemin_suivant: "6A"
chemin_non: "6A"
---