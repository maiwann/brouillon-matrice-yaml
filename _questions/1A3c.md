---
layout: question_oui_non
type: oui_non
code_api: "1A3c"
libelle: "Il est obligatoire de partager un contenu pour accéder à un service."
description: "L'utilisateur a l'obligation de partager le contenu pour y accéder ou accéder à un service : le partage perd donc tout sens éditorial et la liberté de l'utilisateur est fortement contrainte."
exemples: "- Instagram : Partage des posts pour participer à un concours"
score: "10"
chemin_suivant: "1A4"
chemin_non: "1A5"
---