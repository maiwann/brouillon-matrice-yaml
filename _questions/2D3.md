---
layout: question_oui_non
type: oui_non
code_api: "2D3"
libelle: "Le service adopte une esthétique de jeu vidéo."
description: "Nous définissons une esthéthique de jeu vidéo comme l'usage d'images, de couleurs (ou d'agencements de couleurs), d'éléments visuels tels que la pixelisation, ou de typographie qui renvoie consciemment ou inconsciemment à l'univers du jeu vidéo."
exemples: "- Google Maps, programme local guide : la pixelisation de la police renvoie aux jeux vidéos des années 90"
score: "20"
chemin_suivant: "2F"
chemin_non: "2F"
---