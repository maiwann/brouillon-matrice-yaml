---
layout: question_oui_non
type: oui_non
code_api: "9"
libelle: "Le service propose un système de recommandation."
chemin_suivant: "9A"
chemin_non: "10A1"
---