---
layout: question_oui_non
type: oui_non
code_api: "8C"
libelle: "Le service utilise des outils d'analyse permettant de suivre le comportement des curseurs de son audience."
description: "Définition du mouse-tracking : il s'agit de capturer la circulation et les clics de la souris de l'utilisateur sur l'interface.
Les outils de mouse-tracking collectent des données sur l'utilisateur. Cela permet d'orienter le développement de l'interface. Si l'usage de la fonctionnalité parait justifiée en phase de test pour identifier des bugs, elle ne l'est pas en production."
exemples: "Autre proposition : Le service utilise des outils de mouse-tracking (définition)"
score: "90"
chemin_suivant: "8D"
chemin_non: "8D"
---