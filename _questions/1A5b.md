---
layout: question_multiple

type: multiple
code_api: "1A5b"
libelle: "Concernant les notifications secondaires (non liées au fonctionnement du service), la personne utilisatrice... :"

chemin_suivant: "1A5c"

description: "Nous regroupons ici les notifications qui ne concernent pas directement le contenu proposé par le service :
- Sous-entend que la fonctionnalité de notification n'a pas été activée automatiquement : 0%
- L'acceptation des notifications a été déclenchée suite à une proposition du service à l'utilisateur : 30%
- Les notifications ont été mises en place automatiquement sans le consentement de l'utilisateur : 100%"

exemples: "- Contenus publicitaires, 
- Facebook : Notifications « anniversaires »"
reponses: 
- libelle: "a explicitement demandé à les recevoir."
  score: 0
- libelle: "a accepté la proposition de les recevoir."
  score: 30
- libelle: "n'a ni demandé, ni consenti à les recevoir."
  score: 100
---