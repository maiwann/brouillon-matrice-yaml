---
layout: question_oui_non
type: oui_non
code_api: "1A6"
libelle: "Le service affiche un fil d'actualités."
description: "Définition d'un fil d'actualité : recouvre toute interface visant à proposer de façon chronologique ou non une liste d'extraits de contenus dont l'utilisateur peut légitimement supposer qu'ils sont d'actualité (c'est-à-dire récent). Cela recouvre les fils de médias sociaux, aussi bien qu'une liste d'articles à la une d'un média. 

Nous ne considérons pas que le fil d'actualité est problématique en soi mais certaines fonctionnalités de fil d'actualité peuvent l'être."
chemin_suivant: "2A"
chemin_non: "2A"
---