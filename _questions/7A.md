---
layout: question_oui_non
type: oui_non
code_api: "7A"
libelle: "Il est obligatoire de remplir des captchas. **(ajouter définition de captcha)**"
description: "Définition d'un captcha : un captcha est un système de sécurité anti-robot demandant à l'utilisateur de compléter des informations (retranscrire un texte, identifier des formes).

Par défaut, nous considérons que les captchas ne sont pas problématiques, et que leur utilisation est parfois incontournable pour le service."
chemin_suivant: "7A1"
chemin_non: "7B"
---