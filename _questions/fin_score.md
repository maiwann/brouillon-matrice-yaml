---
layout: default
styles:
    - href: "main.css"
---

<section id="fin_score">

  <h2>Le score est de <strong><output></output>.</strong></h2>

<hr>
  <section>
    <div>Score A : Si note < 250 </div>
    <div>Score B : Entre 250 et 500</div>
    <div>Score C : Entre 500 et 750</div>
    <div>Score D : Entre 750 et 1000</div>
    <div>Score E : Entre 1000 et 1500</div>
    <div>Score F : Entre 1500 et 2000</div>
    <div>Score G : Si note > 2000 </div>
  </section>


</section>
