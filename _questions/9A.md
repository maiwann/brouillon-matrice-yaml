---
layout: question_oui_non
type: oui_non
code_api: "9A"
libelle: "Le service utilise un système de recommandation fondé sur des données produites par l'audience."
description: "Définition de système de recommandation collaborative : système dans lequel la recommandation est basée sur des données ou des contenus produits par les utilisateurs du service. Nous considérons que ce type de recommandation nécessite une participation active des utilisateurs pour être efficace et qu'il entraine une incitation des concepteurs à la mise en oeuvre de design persuasif dans ce but."
score: "20"
chemin_suivant: "9A1"
chemin_non: "10A1"
---