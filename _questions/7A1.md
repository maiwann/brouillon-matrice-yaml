---
layout: question_oui_non
type: oui_non
code_api: "7A1"
libelle: "Le captcha demande à réaliser des actions dont le résultat sert à alimenter des données externes."
description: "Le captcha est problématique dans le cas où il demande à réaliser des actions dont le résultat sert à alimenter des algorithmes et/ou données externes. Il y a alors une incitation à faire produire des données par l'utilisateur"
exemples: "- reCaptcha : le captcha de Google sert à alimenter les IA de Google"
score: "30"
chemin_suivant: "7B"
chemin_non: "7B"
---