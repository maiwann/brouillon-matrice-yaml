# coding: utf-8

import os
import csv
import json

import requests
from airtable import airtable

from os.path import join, dirname
from dotenv import load_dotenv

def get_env():
   # Récupérer les variables d'environnement (API Airtable)
	env = join(dirname(__file__), '.env')
	load_dotenv(env)

def get_ressources():
	api = airtable.Airtable(os.getenv('AIRTABLE_BASE_ID'), os.getenv('AIRTABLE_API_KEY'))
	records = api.get(os.getenv('AIRTABLE_TABLE'))

	ressources = records['records']

	while records.get('offset'):
		records = api.get(
			os.getenv('AIRTABLE_TABLE'),
			offset=records.get('offset')
		)
		ressources += records['records']

	# ,limit=10
	return ressources

def get_line(id, fieldname, fields):
	if fieldname in fields:
		return '{}: "{}"\n'.format(id, str(fields[fieldname]).strip())
	return ''

def print_ressources(ressources):
	for ressource in ressources:
		fields = ressource['fields']
		# filename CodeApiDeLaQuestion.md (ex: 1A5f2.md)
		filename = "_questions/"+fields['Code API']+".md"
		# print(filename)
		file = open(filename, "w")
		# print(ressource['fields'])

		if 'Type de question' in ressource['fields']:
			print(filename + " is " + ressource['fields']['Type de question'])

			if ressource['fields']['Type de question'] == "OUI/NON":
				# marquage yaml début
				file.write('---\n')

				# layout: question_oui_non
				file.write('layout: question_oui_non\n')

				# type: oui_non
				file.write('type: oui_non\n')
				# code_api: Code API
				file.write(get_line('code_api', 'Code API', fields))

				# libelle: Proposition Wording Inclusif Yuna
				file.write(get_line('libelle', 'Proposition Wording inclusif Yuna', fields))
				# description: Description
				file.write(get_line('description', 'Description', fields))

				# exemples: Exemples
				file.write(get_line('exemples', 'Exemples', fields))

				# score: Importance
				file.write(get_line('score', 'Importance', fields))
				# chemin_suivant: chemin_suivant
				file.write(get_line('chemin_suivant', 'chemin_suivant', fields))
				# chemin_non: chemin_non
				file.write(get_line('chemin_non', 'chemin_non', fields))

				# marquage yaml fin
				file.write('---')

			if ressource['fields']['Type de question'] == "Choix multiple":
				# marquage yaml début
				file.write('---\n')

				# layout: question_multiple
				file.write('layout: question_multiple\n\n')

				# type: multiple
				file.write('type: multiple\n')
				# code_api: Code API
				file.write(get_line('code_api', 'Code API', fields))
				# libelle: Proposition Wording Inclusif Yuna
				file.write(get_line('libelle', 'Proposition Wording inclusif Yuna', fields))
				file.write("\n")

				# chemin_suivant: chemin_suivant
				file.write(get_line('chemin_suivant', 'chemin_suivant', fields))
				file.write("\n")

				# description: Description
				file.write(get_line('description', 'Description', fields))
				file.write("\n")

				# exemples: Exemples
				if 'Exemples' in ressource['fields']:
					file.write('exemples: "{}"\n'.format(ressource['fields']['Exemples']))
				# reponses: Réponses multiples
				if 'Réponses multiples' in ressource['fields']:
					file.write('reponses: \n{}\n'.format(ressource['fields']['Réponses multiples']))

				# marquage yaml fin
				file.write('---')

		file.close()


get_env()
ressources = get_ressources()
print_ressources(ressources)
