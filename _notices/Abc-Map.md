---
layout: notice

nom: "Abc-Map"
date_creation: "Jeudi, 26 août, 2021 - 14:17"
date_modification: "Jeudi, 26 août, 2021 - 21:30"
logo: 
    src: "../images/logo/Abc-Map.png"
site_web: "https://abc-map.fr"
plateformes: "GNU/Linux, BSD, Mac OS X, Windows"
langues: "Français"
description_courte: "Abc-Map permet de créer des cartes géographiques simplement: importez, dessinez, visualisez des données et +"
createurices: ""
licences: "Licence Publique Générale Affero (AGPL)"
tags: 
    - "Création"
    - "cartographie"
    - "géographie"
    - "éducation"
    - "carte géographique"
    - "SIG"
    - "statistique"
    - "OSM"
lien_wikipedia: ""
lien_exodus: ""
---

Abc-Map permet de manipuler des données géographiques simplement et sans connaissances techniques avancées. Ce logiciel est disponible en ligne et s’utilise avec un navigateur web, sans installation.
Des vidéos  courtes expliquent le fonctionnement du logiciel (
