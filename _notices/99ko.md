---
layout: notice

nom: "99ko"
date_creation: "Jeudi, 4 avril, 2019 - 12:58"
date_modification: "Lundi, 10 mai, 2021 - 13:46"
logo: 
    src: "../images/logo/99ko.png"
site_web: "https://99ko.tuxfamily.org"
plateformes: "GNU/Linux, BSD, Mac OS X, Windows, Android, FirefoxOS, Windows Mobile, Apple iOS, Autre"
langues: "Français"
description_courte: "CMS simple sans base de données"
createurices: "Jonathan C."
licences: "Licence Publique Générale GNU (GNU GPL)"
tags: 
    - "CMS"
    - "blog"
    - "création de site web"
lien_wikipedia: ""
lien_exodus: ""
---

99ko est un CMS simple et sans base de données écrit en PHP. Il permet de créer votre site vitrine ou blog, le tout très rapidement. Ce CMS supporte des plugins et des thèmes. Il est l'anti usine à gaz, mais offre toutefois de beaucoup de possibilités de personnalisation.
Par défaut il embarque des plugins permettant de créer des pages, des sous pages, un blog, un formulaire de contact et une galerie photos. Son thème par défaut est sobre et efficace, il peut servir de base au développement de votre propre thème !

