---
layout: notice

nom: "0 A.D."
date_creation: "Vendredi, 30 décembre, 2016 - 23:38"
date_modification: "Samedi, 27 janvier, 2018 - 23:21"
logo: 
    src: "../images/logo/0 A.D..png"
site_web: "https://play0ad.com/"
plateformes: "GNU/Linux, BSD, Mac OS X, Windows"
langues: "Multilingue"
description_courte: "Jeux de stratégie en temps réel qui respecte un certain réalisme historique et graphique."
createurices: ""
licences: "Creative Commons -By-Sa, Licence Publique Générale GNU (GNU GPL)"
tags: 
    - "Jeux"
    - "RTS"
    - "stratégie"
lien_wikipedia: "https://fr.wikipedia.org/wiki/0_A.D."
lien_exodus: ""
---

0 A.D. - Empires Ascendant est un jeux de stratégie en temps réel. Le principe est simple: récolte des ressources, construit une civilisation et lève une armée pour conquérir les civilisations adversaires. Il est possible de jouer contre l'intelligence artificielle ou contre d'autres personnes via le réseau. Les joueurs souhaitant s'affronter n'ont pas besoin d'utiliser le même système d'exploitation, cependant il est nécessaire que le jeu ait le même numéro de version. Bien qu'il soit en version alpha, le jeu est tout à fait jouable.
Une nouvelle version sort environs tous les 6 mois.

