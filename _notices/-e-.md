---
layout: notice

nom: "/e/"
date_creation: "Jeudi, 16 mai, 2019 - 10:46"
date_modification: "Mercredi, 12 mai, 2021 - 15:37"
logo: 
    src: "../images/logo/-e-.png"
site_web: "https://e.foundation/"
plateformes: "Android"
langues: "Français, Multilingue, English, German, Español, Autre, Non défini"
description_courte: "Un système d'exploitation pour smartphones respectueux de vos données personnelles."
createurices: "Gaël Duval"
licences: "Creative Commons -By-Sa"
tags: 
    - "Système"
    - "Android"
    - "système d'exploitation (OS)"
lien_wikipedia: "https://fr.wikipedia.org/wiki//e/"
lien_exodus: ""
---

/e/ est un système d'exploitation libre pour smartphones. Basé sur LineageOS au début du projet lancé en 2017. A ce jour, /e/ développe son propre OS toujours basé sur Android.
/e/ a pour particularité de n'inclure aucune application Google normalement installées sous Android. Toutes les applications par défaut sont issues du monde libre ou développées en interne. Le système d'exploitation Android est totalement nettoyé des pompes à données de Google. C'est un travail faramineux et minutieux qui a duré plus de 2 ans et qui porte cet OS à un niveau élevé en matière de protection de données personnelles.
En effet, Google est le plus gros contributeur au projet Open Source Android AOSP et n'hésite pas à dissiper, ça et là, dans le code, des moyens techniques pour récupérer les données personnelles des utilisateurs.
/e/OS se veut accessible aux utilisateurs néophytes : l'interface se doit d'être simple, un magasin d'applications est inclus et un service en ligne (cloud) optionnel

